$( document ).ready(function() {
    // Init scrolling table with fixed columns
    var table = $('#tableHistory').DataTable( {
        scrollY: "1000px",
        scrollX: true,
        scrollCollapse: true,
        searching: false,
        ordering: false,
        paging: false,
        info: false,

        // FIXED COLUMN WILL BREAK EXPANDABLE ROW

        // fixedColumns:   {
        //     leftColumns: 0,
        //     rightColumns: 2,
        // }
    });
    
    // Expandable row
    $('#tableHistory tbody tr.clickable-row').on('click', function () {
        var expandableRow = $(this).nextUntil('tr.clickable-row');

        if($(expandableRow).is(":visible")){
            $(expandableRow).hide();
        }
        else{
            $(expandableRow).show();
        }
    });
});