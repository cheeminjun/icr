$( document ).ready(function() {
    // Init scrolling table with fixed columns
    var table = $('#tablePayment').DataTable( {
        scrollY: "1000px",
        scrollX: true,
        scrollCollapse: true,
        searching: false,
        ordering: false,
        paging: false,
        info: false,

        // FIXED COLUMN WILL BREAK EXPANDABLE ROW

        // fixedColumns:   {
        //     leftColumns: 1,
        //     rightColumns: 1,
        // }
    });

    // Custom checkbox
    $('.checkbox').addClass('active');

    $('.checkbox').click(function(event){
        event.stopPropagation();
        $(this).toggleClass('active');
    });

    // Expandable row
    $('#tablePayment tbody tr.clickable-row').on('click', function () {
        var expandableRow = $(this).nextUntil('tr.clickable-row');

        if($(expandableRow).is(":visible")){
            $(expandableRow).hide();
        }
        else{
            $(expandableRow).show();
        }
    });
});