$( document ).ready(function() {
    
    $('#birthDate').datepicker({
        format: 'dd/mm/yyyy',
    }).on('show', function(e) {
        $('body').css( "overflow", "auto" );
    });

    $("#nextButton1").click(function(event){
        var user = $('#studentID').val();
        var password = $('#birthDate').val();

        if (user =="") {
            $('.error').html('Please enter Student ID.');
            event.preventDefault();
        }
        else if (password ==""){
            $('.error').html('Please enter date of birth.');
            event.preventDefault();
        }
        else {
            // Your step 1 verification here

            // If success, load next page
            $('.error').html('');
            location.href = "first-time-2.html";
        }
    });
    
});
