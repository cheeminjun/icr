$( document ).ready(function() {

    var loginCondition = true; // set rules for checking login here

    // if logged in, do this
    if (loginCondition){
        // Logo href
        $('.logo img').click(function(event){
            location.href = "payment-advices.html";
        });
        
        // Insert menu
        $('.welcome').append('<span class="menu no-highlight"><i class="icon ion-android-menu"></i></span><div class="menu-dropdown"><ul><li id="menuPayment">Outstanding</li><li id="menuTransaction">Paid</li><li id="menuPassword">Change Password</li><li id="menuHelp">Help</li><li id="menuLogout">Logout</li></ul></div>');

        // Menu clicks
        $(document).click(function(event) {
            $('.welcome .menu-dropdown').hide();
        });
        $('.welcome .menu').click(function(event){
            event.stopPropagation();
            $('.welcome .menu-dropdown').toggle();
        });
        $('.welcome .menu-dropdown').click(function(event) {
            event.stopPropagation();
        });
        $('#menuPayment').click(function(event){
            location.href = "payment-advices.html";
        });
        $('#menuTransaction').click(function(event){
            location.href = "transaction-history.html";
        });
        $('#menuPassword').click(function(event){
            location.href = "change-password.html";
        });
        $('#menuHelp').click(function(event){
            location.href = "help.html";
        });
        $('#menuLogout').click(function(event){
            // Your logout code here
        });
    }
    //if not logged in, do this
    else{
        // Insert menu
        $('.welcome').append('<span class="menu no-highlight"><a href="index.html">Back to Login</a></span>');

        // Logo href
        $('.logo img').click(function(event){
            location.href = "index.html";
        });
    }
    
});
