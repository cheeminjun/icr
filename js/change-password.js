$( document ).ready(function() {
    
    $("#changeButton").click(function(event){
        var currentPassword = $('#currentPassword').val();
        var newPassword = $('#newPassword').val();
        var confirmPassword = $('#confirmPassword').val();

        if (currentPassword =="") {
            $('.error').html('Please enter current password.');
            event.preventDefault();
        }
        else if (newPassword ==""){
            $('.error').html('Please enter new password.');
            event.preventDefault();
        }
        else if (confirmPassword ==""){
            $('.error').html('Please confirm new password.');
            event.preventDefault();
        }
        else {
            // Your password update code here

            // If success, clear form
            $('.status').html('Password successfully changed!');
            $('.error').html('');
            $('.form-control').val('');
        }
    });

});