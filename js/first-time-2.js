$( document ).ready(function() {

    $("#setButton").click(function(event){
        var newPassword = $('#newPassword').val();
        var confirmPassword = $('#confirmPassword').val();

        if (newPassword ==""){
            $('.error').html('Please enter new password.');
            event.preventDefault();
        }
        else if (confirmPassword ==""){
            $('.error').html('Please confirm new password.');
            event.preventDefault();
        }
        else {
            // Your password set code here

            // If success, load next page
            $('.error').html('');
            location.href = "payment-advices.html";
        }
    });

});
