$( document ).ready(function() {

    $("#nextButton2").click(function(event){
        var otp = $('#otp').val();

        if (otp =="") {
            $('.error').html('Please enter One Time Password.');
            event.preventDefault();
        }
        else {
            // Your step 2 verification here

            // If success, load next page
            $('.error').html('');
            location.href = "forgot-password-3.html";
        }
    });
    
});
