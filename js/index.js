$( document ).ready(function() {
    
    $("#loginButton").click(function(event){
        var user = $('#loginUser').val();
        var password = $('#loginPassword').val();

        if (user =="") {
            $('.error').html('Please enter Student ID.');
            event.preventDefault();
        }
        else if (password ==""){
            $('.error').html('Please enter password.');
            event.preventDefault();
        }
        else {
            // Your login verification here

            // If success, load Payment Advices
            $('.error').html('');
            location.href = "payment-advices.html";
        }
    });
    
});
